// dummy data for book detail
const bookData = {
  title: "Lost Man",
  author: "xyz",
  publicationYear: "2020",
};

class Book {
  bookDetail = {};

  getBookDetails() {
    return this.bookDetail;
  }

  setBookDetails(bookDetail) {
    this.bookDetail = bookDetail;
  }
}

const lostMan = new Book();

lostMan.setBookDetails(bookData);

console.log(lostMan.getBookDetails());
