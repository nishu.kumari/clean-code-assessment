// dummy data for books
const booksData = [
  {
    title: "Lost Man",
    author: "xyz",
    publicationYear: "2020",
  },
  {
    title: "ghjhjh",
    author: "xyz",
    publicationYear: "2020",
  },
  {
    title: "hyfyf",
    author: "abc",
    publicationYear: "2020",
  },
];

class BookLibrary {
  bookList = [];

  addBookInLibrary = (bookDetail) => {
    this.bookList.push(bookDetail);
  };

  getTotalNumberOfBooks = () => {
    return this.bookList.length;
  };

  searchBookByTitle = (searchKey) => {
    return this.bookList.filter((book) => book.title.includes(searchKey));
  };

  searchBookByAuthor = (searchKey) => {
    return this.bookList.filter((book) => book.author.includes(searchKey));
  };
}

const bookLibrary = new BookLibrary();
bookLibrary.addBookInLibrary(booksData[0]);
bookLibrary.addBookInLibrary(booksData[1]);
bookLibrary.addBookInLibrary(booksData[2]);

//USING THE METHODS THAT WE HAVE CREATED IN CLASS
console.log(bookLibrary.searchBookByAuthor("abc"));
console.log(bookLibrary.getTotalNumberOfBooks());
